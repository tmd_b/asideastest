#!/bin/bash


DEFPORT=8081
PORT=$1
appname="happy-stars.jar"

if [ -z $PORT ]
then
	echo "No port given"
	echo "Starting on port $DEFPORT"
	java -jar -Dserver.port=$DEFPORT $appname
	
else
	if ! [[ $PORT =~ ^-?[0-9]+$ ]]
    then
       	echo "Argument needs to be a port number"
		echo "Starting on default port $DEFPORT"
		java -jar -Dserver.port=$DEFPORT $appname
		
	else
		echo "Starting on port $PORT"
		java -jar -Dserver.port=$PORT $appname
	fi
fi
